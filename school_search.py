import csv, time

class SchoolData():
    """
    Name :
    SchoolData

    Description :
    This class reads data from school data csv to search schools.
    Hash base search used to implement to search schools
    State --> City --> Schools

    Attributes:
        schools_data :  Dict to store csv data
        curr_state:     To save current matching state to the query
        curr_city:      To save current matching city to the query
        results:        To save matching results
        location_score: To save location scoring result on matching query
        school_score:   To save school score on matching query
        newline:    New line
        NCESSCH:    CSV header index
        SCHNAM:     CSV school name column index
        LCITY:      CSV City name column index
        LSTATE:     CSV state name column index
        MLOCALE:    CSV Metro Locale column index
    Functions:


    search_schools
        Takes search query as input.
        For location match scorting is 1.
        For earch school name match scoring is 5.

    search_location
        Check for location (State or city) match from query
        On matching location scoting is 1.

    search_school_name
        Check for school name match from query
        Scoring on each keyword matching is 5.

    Args:
        Search Query and School Data.

        Return
            Top 3 search result to the matching query
    """
    def __init__(self):

        self.schools_data = dict()
        self.curr_state = ''
        self.curr_city = ''
        self.results = list()
        self.location_score = 0
        self.school_score = 0
        self.newline = '\n'

        # Columns No. to map read data from csv
        NCESSCH = 0
        SCHNAM = 3      # School Name Column Header
        LCITY = 4
        LSTATE = 5
        MLOCALE = 8

        # Read School data from CSV File
        with open('school_data.csv', encoding='cp1252') as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                # header check and skip
                if row[0] == 'NCESSCH':
                    continue

                if row[LSTATE] not in self.schools_data:
                    self.schools_data[row[LSTATE]] = dict()

                if row[LCITY] not in self.schools_data[row[LSTATE]]:
                    self.schools_data[row[LSTATE]][row[LCITY]]= list()

                self.schools_data[row[LSTATE]][row[LCITY]].append(row[SCHNAM])

    def __str__(self):
        return '<SchoolData>\n{}'.format(self.SchoolData)

    #   state--->city--->school
    def search_location(self, node,search_string):
        for k,v in node.items():

            if len(k) == 2:
                self.curr_state = k
            else:
                self.curr_city = k

            # reset score
            self.location_score = 0

            # check in location node
            for i in [x.strip().upper() for x in search_string.split(' ')]:
                if i in k:
                    self.location_score+=1  # location match scoring increment
            if isinstance(v,dict):
                self.search_location(v,search_string)
            elif isinstance(v,list):
                self.search_school_name(v,search_string)

    # Schools Name search
    def search_school_name(self, node, search_string):
        self.school_score = 0
        for school_name in node:
            for i in [x.strip().upper() for x in search_string.split(' ')]:
                if i == 'SCHOOL':
                    continue
                # scoring based on result match
                if i in school_name and i not in self.curr_city and i not in self.curr_state:
                    self.school_score+=5
                else:
                    self.school_score-=1

            if self.school_score > 0 or self.location_score > 0:
                total_score = self.school_score + self.location_score
                self.results.append((total_score,'{} \n{}, {}'.format(school_name,self.curr_city,self.curr_state)))

            self.school_score = 0


    def search_schools(self,query):
        start_ms = int(round(time.time() * 1000))   # Start timer

        self.search_location(self.schools_data,query)

        stop_ms = int(round(time.time() * 1000))    # stop timer

        results = sorted(self.results)[-3:]
        result_time = (stop_ms - start_ms)/1000.0
        print(f'Results for "{query}" (search took: {result_time}s)')      # Print results as per requirements
        print (self.newline.join([f'{index+1}. {x[1]}' for index, x in enumerate(results[::-1])]))
        self.results = list()


if __name__ == "__main__":
    # Testing Test case from quetions
    school_search = SchoolData()
    school_search.search_schools('elementary school highland park')
    school_search.search_schools('jefferson belleville')
    school_search.search_schools('riverside school 44')
    school_search.search_schools('granada charter school')
    school_search.search_schools('foley high alabama')
    school_search.search_schools('KUSKOKWIM')
    school_search.search_schools('riverside')
    school_search.search_schools('PRE-K INCENTIVE')