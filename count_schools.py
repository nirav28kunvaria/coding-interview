import csv, time, itertools

class SchoolData():
    """
    Name :
    SchoolData

    Description :
    This class reads data from school data csv to search schools.

    Attributes:
        schools_data :  Dict to store csv data
        school_total:   TO save total no of schools
        school_total_by_state:  Dict to save schools state
        school_total_by_metro:  Dict to store schools metro locale
        school_total_by_city:   Dict to store schools city
        newline:    New line
        NCESSCH:    CSV header index
        SCHNAM:     CSV school name column index
        LCITY:      CSV City name column index
        LSTATE:     CSV state name column index
        MLOCALE:    CSV Metro Locale column index

    Functions:

    get_total_schools
        Return Total No of Schools
    get_schools_by_state
        Return list of schools by state in sorted order.
    get_schools_by_metro
        Return list of schools by Metro centric locale in sorted order.
    get_city_most_schools
        Return list of most schools in city in sorted order.
    get_city_min_one_school
        Return Unique cities with at least one school.
    print_count
        Return Prints all self functions results.

    """
    def __init__(self):
        self.schools_data = dict()
        self.school_total = 0
        self.school_total_by_state = dict()
        self.school_total_by_metro = dict()
        self.school_total_by_city = dict()
        self.newline = '\n'

        # Columns No. to map read data from csv
        NCESSCH = 0
        SCHNAM = 3      # School Name Column Header
        LCITY = 4
        LSTATE = 5
        MLOCALE = 8

        # Read School data from CSV File
        with open('school_data.csv', encoding='cp1252') as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                # header check and skip
                if row[0] == 'NCESSCH':
                    continue

                if row[LSTATE] not in self.schools_data:
                    self.schools_data[row[LSTATE]] = dict()
                    self.school_total_by_state[row[LSTATE]] = 0
                    # print('State added: {}').format(row[LSTATE])
                if row[MLOCALE] not in self.schools_data[row[LSTATE]]:
                    self.schools_data[row[LSTATE]][row[MLOCALE]] = dict()
                    self.school_total_by_metro[row[MLOCALE]] = 0
                    # print('Locale added: {}').format(row[MLOCALE])
                if row[LCITY] not in self.schools_data[row[LSTATE]][row[MLOCALE]]:
                    self.schools_data[row[LSTATE]][row[MLOCALE]][row[LCITY]] = list()
                    self.school_total_by_city[row[LCITY]] = 0
                    # print('LCity added: {}').format(row[LCITY])


                self.schools_data[row[LSTATE]][row[MLOCALE]][row[LCITY]].append(row[NCESSCH])
                self.school_total +=1
                self.school_total_by_state[row[LSTATE]] +=1
                self.school_total_by_metro[row[MLOCALE]] +=1
                self.school_total_by_city[row[LCITY]] +=1

    def __str__(self):
        return f'SchoolData {self.newline}{self.SchoolData}'

    def get_total_schools(self):
        """
        Description 
            Function to get total Schools from csv data
        Return
            Total No of Schools
        """
        return f"Total schools: {self.school_total}"

    def get_schools_by_state(self):
        """
        Description
            State total Schools
        Return
            list of schools by state in sorted order
        """
        return f"{self.newline}Schools by state:{self.newline}{self.newline.join([f'{i}: {self.school_total_by_state[i]}' for i in sorted(self.school_total_by_state)])}"

    def get_schools_by_metro(self):
        """
        Description
            Metro Locale Schools count
        Return
            list of schools by Metro centric locale in sorted order
        """
        return f"{self.newline}Schools by metro locale:{self.newline}{self.newline.join([f'{i}: {self.school_total_by_metro[i]}' for i in sorted(self.school_total_by_metro)])}"

    def get_city_most_schools(self):
        """
        # Return list of most schools in city in sorted order
        """
        most_schools = sorted(self.school_total_by_city.items(), key=lambda kv: kv[1])[len(self.school_total_by_city)-1]
        return f"{self.newline}City with most schools: {most_schools[0]} ({most_schools[1]} Schools)"

    def get_city_min_one_school(self):
        """
        Description
            City with only one school
        Return
            No. of Unique cities with at least one school
        """
        return f"{self.newline}Unique cities with at least one school: {len(self.school_total_by_city)}"


    def print_counts(self):
        """
        Description
            To Print all self functions results
        """
        print(self.get_total_schools())
        print(self.get_schools_by_state())
        print(self.get_schools_by_metro())
        print(self.get_city_most_schools())
        print(self.get_city_min_one_school())


if __name__ == "__main__":
    # Testing Test case from quetions
    schools = SchoolData()
    schools.print_counts()